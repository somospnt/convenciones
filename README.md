# Convenciones

Reglas de PMD para los proyectos del equipo.

## Cómo usar

Pegá esto en tu `pom.xml`
- Si querés tener siempre las reglas más recientes (recomendado si tu proyecto no es muy grande):
```xml
    <build>
        ...
        <plugins>
            ...
            
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-pmd-plugin</artifactId>
                <version>3.11.0</version>
                <executions>
                    <execution>
                        <id>validate</id>
                        <phase>validate</phase>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <printFailingErrors>true</printFailingErrors>
                    <rulesets>
                        <ruleset>https://gitlab.com/somospnt/convenciones/raw/master/pmd_rules.xml</ruleset>
                    </rulesets>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <version>3.0.0</version>
                <executions>
                    <execution>
                        <id>validate</id>
                        <phase>validate</phase>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <configLocation>https://gitlab.com/somospnt/convenciones/raw/master/checkstyle_rules.xml</configLocation>
                </configuration>
            </plugin>

            ...
        </plugins>
        ...
    </build>
```

- Si querés usar una versión específica de las reglas (recomendado si tenés un proyecto muy grande):
```xml
    <build>
        ...
        <plugins>
            ...

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-pmd-plugin</artifactId>
                <version>3.11.0</version>
                <executions>
                    <execution>
                        <id>validate</id>
                        <phase>validate</phase>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <printFailingErrors>true</printFailingErrors>
                    <rulesets>
                        <ruleset>https://gitlab.com/somospnt/convenciones/raw/1.0.0/pmd_rules.xml</ruleset>
                    </rulesets>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <version>3.0.0</version>
                <executions>
                    <execution>
                        <id>validate</id>
                        <phase>validate</phase>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <configLocation>https://gitlab.com/somospnt/convenciones/raw/1.0.0/checkstyle_rules.xml</configLocation>
                </configuration>
            </plugin>

            ...
        </plugins>
        ...
    </build>
```

- Si se quiere usar checkstyle y pmd para reporte usar lo siguiente
```xml
    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-project-info-reports-plugin</artifactId>
                <version>3.0.0</version>
                <configuration>
                    <dependencyLocationsEnabled>false</dependencyLocationsEnabled>
                </configuration>
                <reportSets>
                    <reportSet>
                        <reports>
                            <report>index</report>
                        </reports>
                    </reportSet>
                </reportSets>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jxr-plugin</artifactId>
                <version>3.0.0</version>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <version>3.0.0</version>
                <configuration>
                    <configLocation>https://gitlab.com/somospnt/convenciones/raw/master/checkstyle_rules.xml</configLocation>
                </configuration>
                <reportSets>
                    <reportSet>
                        <reports>
                            <report>checkstyle</report>
                        </reports>
                    </reportSet>
                </reportSets>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-pmd-plugin</artifactId>
                <version>3.12.0</version>
                <configuration>
                    <printFailingErrors>true</printFailingErrors>
                    <rulesets>
                        <ruleset>https://gitlab.com/somospnt/convenciones/raw/master/pmd_rules.xml</ruleset>
                    </rulesets>
                </configuration>
            </plugin>
        </plugins>
    </reporting>
```xml